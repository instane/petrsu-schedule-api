.PHONY: install scratch-install up down ps mycli composer composer-install env console

all: up

install: composer-install up

scratch-install: env install

up:
	@docker-compose up -d reverse-proxy

down:
	@docker-compose down

ps:
	@docker-compose ps

mycli:
	@docker-compose run mycli

composer:
	@docker-compose run composer $(filter-out $@,$(MAKECMDGOALS))

composer-install:
	@docker-compose run composer install

console:
	@docker-compose exec php7 php bin/console $(filter-out $@,$(MAKECMDGOALS))

env:
	@cp .env.dist .env
